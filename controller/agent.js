var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');
var password = require("../helper/password.js");
var ObjectId = require('mongodb').ObjectID;
var send_sms = require('../helper/send_sms.js');
var agentValidator = require('../validator/agent.js');
var driverValidator = require('../validator/driver.js');


module.exports = function(db) {
    return {
        //these are the driver routes 
        addAgent: function(req, res) {
            req.checkBody(agentValidator);
            if (!req.validateAndRespond()) return;
            var agentData = req.body.agent;
            agentData.status = "inactive";

            password.hashPassword(agentData.password, function(_password) {
                console.log(_password);
                var _token = agentData.email + agentData.password;
                agentData.token = jwt.sign(_token, (new Date()).toString());
                agentData.password = _password;
                ///OTP///
                var speakeasy = require('speakeasy');
                var secret = speakeasy.generateSecret({ length: 20 });

                var otp = speakeasy.totp({
                    secret: secret.base32,
                    encoding: 'base32'
                });
                agentData.otp = otp;
                db.collection('agent').insertOne(agentData, function(err, result) {
                    console.log(err);
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        send_sms.sendSMS(agentData.mobile, agentData.otp).then(function(data) {
                            res.send({
                                err: false,
                                result: result
                            });
                        });
                    }
                });
            });

        },
        getAgent: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('agent').findOne({ _id: new ObjectId(id) }, { password: 0, _id: 0 }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "driver not found with given id"
                    });
                } else {
                    res.send({
                        err: false,
                        result: {
                            user: result
                        }
                    });
                }
            });
        },
        forgotpasswordSMS: function(req, res) {
            if (!req.validateAndRespond()) return;
            var code = req.body.code;
            var agentData = req.body.agent;
            db.collection('agent').findOne({ code: code }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "Agent not found with given Code"
                    });
                } else {
                    ///OTP///
                    var speakeasy = require('speakeasy');
                    var secret = speakeasy.generateSecret({ length: 20 });

                    var otp = speakeasy.totp({
                        secret: secret.base32,
                        encoding: 'base32'
                    });
                    result.otp = otp;
                    ///
                    db.collection('agent').update({ "mobile": result.mobile }, { $set: { "otp": otp } }, function(err, DATA) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            console.log(result.mobile);
                            send_sms.sendSMS(result.mobile, result.otp).then(function(data) {
                                res.send({
                                    err: false,
                                    result: 'otp is send'
                                });
                            });


                        }


                    });
                }
            });
        },
        ChangePassword: function(req, res) {
            if (!req.validateAndRespond()) return;
            var code = req.params.code;

            var otp = req.body.otp;
            var Newpassword = req.body.password;
            var conf_password = req.body.confPassword;
            if (Newpassword !== conf_password) {
                return res.send({
                    err: true,
                    error: "Password and confirm password do not match."
                });

            }
            db.collection('agent').findOne({ otp: otp }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Invalid OTP Presented.Please try again"
                    });
                } else {
                    password.hashPassword(Newpassword, function(_password) {

                        db.collection('agent').update({ otp: otp }, { $set: { "password": _password } }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: err
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: result
                                });
                            }
                        });
                    });

                }
            });
        },
        driverActivate: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;

            // var otp = req.body.otp;
            db.collection('driver').findOne({ _id: new ObjectId(id) }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Invalid OTP Presented.Please try again"
                    });
                } else {

                    db.collection('driver').update({ _id: new ObjectId(id) }, { $set: { "status": "active" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });


                        }
                    });
                }
            });
        },
        deleteAgent: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('agent').findOne({ _id: new ObjectId(id) }, function(err, result) {
                console.log(result);
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                }
                db.collection('agent').remove({ _id: new ObjectId(id) }, function(err, result) {
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        return res.send({
                            err: false,
                            result: result
                        });
                    }
                });
            });
        },
        agentLogin: function(req, res) {
            req.checkBody({
                'agent.password': {
                    notEmpty: true
                },
                'agent.code': {
                    notEmpty: true
                }
            });
            if (!req.validateAndRespond()) return;
            var code = req.body.agent.code;
            db.collection('agent').findOne({ code: code }, function(err, agent) {
                if (agent === null) {
                    return res.send({
                        err: true,
                        error: ["Agent code doesn't exist"]
                    });
                } else {
                    bcrypt.compare(req.body.agent.password, agent.password, function(err, result) {
                        console.log("Bcrypt returns: " + result);
                        if (err) {
                            res.status(501).json({
                                err: false,
                                error: ["Server error"]
                            });
                        } else {
                            if (result) {
                                var _token = req.body.agent.code + req.body.agent.password;
                                var token = jwt.sign(_token, (new Date()).toString());
                                db.collection('agent').update({ _id: new ObjectId(agent._id) }, { $set: { "token": token } }, function(err, result) {
                                    if (err) {
                                        return res.send({
                                            err: true,
                                            error: err
                                        });
                                    } else {
                                        res.json({
                                            err: false,
                                            result: { token: token }
                                        });
                                    }
                                });
                            } else {
                                res.status(401).json({
                                    err: true,
                                    error: ["Invalid AgentCode/password combination"]
                                });
                            }
                        }
                    });
                }
            });
        },
        agentLogout: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;

            var otp = req.body.otp;
            db.collection('agent').findOne({ _id: new ObjectId(id) }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Agent with given id not found"
                    });
                } else {

                    db.collection('agent').update({ _id: new ObjectId(id) }, { $unset: { "token": "" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: "agent logged out successfully."
                            });
                        }
                    });
                }
            });
        },
        deleteDriver: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('driver').findOne({ _id: new ObjectId(id) }, function(err, result) {
                console.log(result);
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                }
                db.collection('driver').remove({ _id: new ObjectId(id) }, function(err, result) {
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        return res.send({
                            err: false,
                            result: result
                        });
                    }
                });
            });
        },
        updateAgent: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('agent').findOne({ _id: new ObjectId(id) }, { password: 0 }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Agent not found with given id"
                    });
                } else {
                    var agentData = req.body.agent;
                    var datenow = new Date();
                    agentData.updatedOn = datenow;
                    db.collection('agent').update({ _id: new ObjectId(id) }, { $set: agentData }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });
                        }
                    });
                }
            });
        },

    }
}