var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');
var password = require("../helper/password.js");
var ObjectId = require('mongodb').ObjectID;
var send_sms = require('../helper/send_sms.js');
//var send_email = require('../helper/send_email.js');
var send_email = require('../helper/sendMail.js');
var agentValidator = require('../validator/admin');
var driverValidator = require('../validator/admin.js');
var adminValidator = require("../validator/admin.js");
var agentValidator = require('../validator/agent.js');

module.exports = function(db) {
    return {
        addAdmin: function(req, res) {
            req.checkBody(adminValidator);
            if (!req.validateAndRespond()) return;
            var adminData = req.body.admin;
            adminData.status = "inactive";
            adminData.adminType = "admin";
            password.hashPassword(adminData.password, function(_password) {
                console.log(_password);
                var _token = adminData.email + adminData.password;
                adminData.token = jwt.sign(_token, (new Date()).toString());
                adminData.password = _password;

                ///OTP///
                var speakeasy = require('speakeasy');
                var secret = speakeasy.generateSecret({ length: 20 });

                var otp = speakeasy.totp({
                    secret: secret.base32,
                    encoding: 'base32'
                });
                adminData.otp = otp;
                ///
                db.collection('admin').insertOne(adminData, function(err, result) {
                    console.log(err);
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        send_sms.sendSMS(adminData.mobile, adminData.otp).then(function(data) {
                            res.send({
                                err: false,
                                result: result
                            });
                        });
                    }
                });
            });
        },
        getAdmin: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('admin').findOne({ _id: new ObjectId(id) }, { password: 0, _id: 0 }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "admin not found with given id"
                    });
                } else {
                    res.send({
                        err: false,
                        result: {
                            user: result
                        }
                    });
                }
            });
        },
        //forget password send mail:OTP
        forgotpassword: function(req, res) {
            //if (!req.validateAndRespond()) return;
            var email = req.body.email;
            var adminData = req.body;
            ///OTP///
            var speakeasy = require('speakeasy');
            var secret = speakeasy.generateSecret({ length: 20 });

            var otp = speakeasy.totp({
                secret: secret.base32,
                encoding: 'base32'
            });
            adminData.otp = otp;
            ///
            db.collection('admin').update({ "email": email }, { $set: { "otp": otp } }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                } else {
                    send_email.Send_Email(adminData.email, 'Forgot Password', 'email_body_forgot', adminData).then(function(data) {
                        res.send({
                            err: false,
                            result: data
                        });
                    });
                }
            });
        },
        //forget password sent SMS :OTP
        forgotpasswordSMS: function(req, res) {
            if (!req.validateAndRespond()) return;
            var mobile = req.body.mobile;
            var adminData = req.body.admin;
            db.collection('admin').findOne({ mobile: mobile }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "Admin not found with given Mobile"
                    });
                } else {
                    ///OTP///
                    var speakeasy = require('speakeasy');
                    var secret = speakeasy.generateSecret({ length: 20 });

                    var otp = speakeasy.totp({
                        secret: secret.base32,
                        encoding: 'base32'
                    });
                    result.otp = otp;
                    ///
                    db.collection('admin').update({ "mobile": result.mobile }, { $set: { "otp": otp } }, function(err, DATA) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            console.log(result.mobile);
                            send_sms.sendSMS(result.mobile, result.otp).then(function(data) {
                                res.send({
                                    err: false,
                                    result: 'otp is send'
                                });
                            });


                        }


                    });
                }
            });
        },
        //change password
        ChangePassword: function(req, res) {
            if (!req.validateAndRespond()) return;
            var email = req.body.email;
            var otp = req.body.otp;
            var Newpassword = req.body.password;
            var conf_password = req.body.confPassword;
            if (Newpassword !== conf_password) {
                return res.send({
                    err: true,
                    error: "Password and confirm password do not match."
                });
            }
            db.collection('admin').findOne({ email: email, otp: otp }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Invalid OTP Presented.Please try again"
                    });
                } else {
                    password.hashPassword(Newpassword, function(_password) {

                        db.collection('admin').update({ email: email }, { $set: { "password": _password } }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: err
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: result
                                });
                            }
                        });
                    });

                }
            });
        },
        adminLogin: function(req, res) {
            req.checkBody({
                'admin.password': {
                    notEmpty: true
                },
                'admin.email': {
                    notEmpty: true
                }
            });
            if (!req.validateAndRespond()) return;
            var email = req.body.admin.email;
            db.collection('admin').findOne({ email: email }, function(err, admin) {
                if (admin === null) {
                    return res.send({
                        err: true,
                        error: ["email doesn't exist"]
                    });
                } else {
                    bcrypt.compare(req.body.admin.password, admin.password, function(err, result) {
                        console.log("Bcrypt returns: " + result);
                        if (err) {
                            res.status(501).json({
                                err: false,
                                error: ["Server error"]
                            });
                        } else {
                            if (result) {
                                var _token = req.body.admin.email + req.body.admin.password;
                                var token = jwt.sign(_token, (new Date()).toString());
                                db.collection('admin').update({ _id: new ObjectId(admin._id) }, { $set: { "token": token } }, function(err, result) {
                                    if (err) {
                                        return res.send({
                                            err: true,
                                            error: err
                                        });
                                    } else {
                                        res.json({
                                            err: false,
                                            result: { token: token }
                                        });
                                    }
                                });
                            } else {
                                res.status(401).json({
                                    err: true,
                                    error: ["Invalid email/password combination"]
                                });
                            }
                        }
                    });
                }
            });
        },
        adminActivate: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;

            var otp = req.body.otp;
            db.collection('admin').findOne({ _id: new ObjectId(id), otp: otp }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Invalid OTP Presented.Please try again"
                    });
                } else {

                    db.collection('admin').update({ _id: new ObjectId(id) }, { $set: { "status": "active" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });
                        }
                    });
                }
            });
        },
        updateAdmin: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('admin').findOne({ _id: new ObjectId(id) }, { password: 0 }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Admin not found with given id"
                    });
                } else {
                    var adminData = req.body.admin;
                    var datenow = new Date();
                    adminData.updatedOn = datenow;
                    db.collection('admin').update({ _id: new ObjectId(id) }, { $set: adminData }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });
                        }
                    });
                }
            });
        },
        adminlogout: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('admin').findOne({ _id: new ObjectId(id) }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "User with given id not found"
                    });
                } else {

                    db.collection('admin').update({ _id: new ObjectId(id) }, { $unset: { "token": "" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: "User logged out successfully."
                            });
                        }
                    });
                }
            });
        },
        agentActivate: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;

            // var otp = req.body.otp;
            db.collection('agent').findOne({ _id: new ObjectId(id) }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Invalid OTP Presented.Please try again"
                    });
                } else {

                    db.collection('agent').update({ _id: new ObjectId(id) }, { $set: { "status": "active" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });


                        }
                    });
                }
            });
        },
        deleteAgent: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('agent').findOne({ _id: new ObjectId(id) }, function(err, result) {
                console.log(result);
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                }
                db.collection('agent').remove({ _id: new ObjectId(id) }, function(err, result) {
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        return res.send({
                            err: false,
                            result: result
                        });
                    }
                });
            });
        },
    };
};