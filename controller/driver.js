var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');
var password = require("../helper/password.js");
var ObjectId = require('mongodb').ObjectID;
var send_sms = require('../helper/send_sms.js');
var driverValidator = require('../validator/driver.js');
var multer = require('multer');
var img = "";

var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './uploads');
    },
    filename: function(req, file, callback) {
        img = "./uploads" + file.fieldname + '-' + Date.now();
        callback(null, file.fieldname + '-' + Date.now());
    }
});
var upload = multer({ storage: storage }).single('file');



module.exports = function(db) {
    return {
        //these are the driver routes 
        addDriver: function(req, res) {
            req.checkBody(driverValidator);
            if (!req.validateAndRespond()) return;
            var driverData = req.body.driver;
            driverData.status = "inactive";
            password.hashPassword(driverData.password, function(_password) {
                console.log(_password);
                var _token = driverData.email + driverData.password;
                driverData.token = jwt.sign(_token, (new Date()).toString());
                driverData.password = _password;

                ///OTP///
                var speakeasy = require('speakeasy');
                var secret = speakeasy.generateSecret({ length: 20 });

                var otp = speakeasy.totp({
                    secret: secret.base32,
                    encoding: 'base32'
                });
                driverData.otp = otp;
                db.collection('driver').insertOne(driverData, function(err, result) {
                    console.log(err);
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        send_sms.sendSMS(driverData.mobile, driverData.otp).then(function(data) {
                            res.send({
                                err: false,
                                result: result
                            });
                        });
                    }
                });
            });

        },
        getDriver: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('driver').findOne({ _id: new ObjectId(id) }, { password: 0, _id: 0 }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "driver not found with given id"
                    });
                } else {
                    res.send({
                        err: false,
                        result: {
                            user: result
                        }
                    });
                }
            });
        },
        driverLogin: function(req, res) {
            req.checkBody({
                'driver.password': {
                    notEmpty: true
                },
                'driver.batchno': {
                    notEmpty: true
                }
            });
            if (!req.validateAndRespond()) return;
            var batchno = req.body.driver.batchno;
            db.collection('driver').findOne({ batchno: batchno }, function(err, driver) {
                if (driver === null) {
                    return res.send({
                        err: true,
                        error: ["batchNumber doesn't exist"]
                    });
                } else {
                    bcrypt.compare(req.body.driver.password, driver.password, function(err, result) {
                        console.log("Bcrypt returns: " + result);
                        if (err) {
                            res.status(501).json({
                                err: false,
                                error: ["Server error"]
                            });
                        } else {
                            if (result) {
                                var _token = req.body.driver.batchno + req.body.driver.password;
                                var token = jwt.sign(_token, (new Date()).toString());
                                db.collection('driver').update({ _id: new ObjectId(driver._id) }, { $set: { "token": token } }, function(err, result) {
                                    if (err) {
                                        return res.send({
                                            err: true,
                                            error: err
                                        });
                                    } else {
                                        res.json({
                                            err: false,
                                            result: { token: token }
                                        });
                                    }
                                });
                            } else {
                                res.status(401).json({
                                    err: true,
                                    error: ["Invalid batchNumber/password combination"]
                                });
                            }
                        }
                    });
                }
            });
        },
        forgotpassword: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            var driverData = req.body;
            ///OTP///
            var speakeasy = require('speakeasy');
            var secret = speakeasy.generateSecret({ length: 20 });

            var otp = speakeasy.totp({
                secret: secret.base32,
                encoding: 'base32'
            });
            driverData.otp = otp;
            ///
            db.collection('driver').update({ _id: new ObjectId(id) }, { $set: { "otp": otp } }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                } else {
                    sendMail.Send_Email(UserData.email, 'Forgot Password', 'email_body_forgot', UserData).then(function(data) {
                        res.send({
                            err: false,
                            result: data
                        });
                    });

                }
            });
        },
        forgotpasswordSMS: function(req, res) {
            if (!req.validateAndRespond()) return;
            var batchno = req.body.batchno;
            var driverData = req.body.driver;
            db.collection('driver').findOne({ batchno: batchno }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "driver not found with given batchno"
                    });
                } else {
                    ///OTP///
                    var speakeasy = require('speakeasy');
                    var secret = speakeasy.generateSecret({ length: 20 });

                    var otp = speakeasy.totp({
                        secret: secret.base32,
                        encoding: 'base32'
                    });
                    result.otp = otp;
                    ///
                    db.collection('driver').update({ "mobile": result.mobile }, { $set: { "otp": otp } }, function(err, DATA) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            console.log(result.mobile);
                            send_sms.sendSMS(result.mobile, result.otp).then(function(data) {
                                res.send({
                                    err: false,
                                    result: 'otp is send'
                                });
                            });


                        }


                    });
                }
            });
        },
        ChangePassword: function(req, res) {
            if (!req.validateAndRespond()) return;
            var batchno = req.params.batchno;

            var otp = req.body.otp;
            var Newpassword = req.body.password;
            var conf_password = req.body.confPassword;
            if (Newpassword !== conf_password) {
                return res.send({
                    err: true,
                    error: "Password and confirm password do not match."
                });

            }
            db.collection('driver').findOne({ otp: otp }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Invalid OTP Presented.Please try again"
                    });
                } else {
                    password.hashPassword(Newpassword, function(_password) {

                        db.collection('driver').update({ otp: otp }, { $set: { "password": _password } }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: err
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: result
                                });
                            }
                        });
                    });

                }
            });
        },
        userActivate: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;

            var otp = req.body.otp;
            db.collection('companyUsers').findOne({ _id: new ObjectId(id), otp: otp }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Invalid OTP Presented.Please try again"
                    });
                } else {

                    db.collection('companyUsers').update({ _id: new ObjectId(id) }, { $set: { "status": "active" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });
                        }
                    });
                }
            });
        },
        driverLogout: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;

            var otp = req.body.otp;
            db.collection('driver').findOne({ _id: new ObjectId(id) }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "driver with given id not found"
                    });
                } else {

                    db.collection('driver').update({ _id: new ObjectId(id) }, { $unset: { "token": "" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: "driver logged out successfully."
                            });
                        }
                    });
                }
            });
        },
        updateDriver: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;

            db.collection('driver').findOne({ _id: new ObjectId(id) }, { password: 0 }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "No Driver found with the provided Id"
                    });
                } else {
                    var driverData = req.body.driver;
                    var datenow = new Date();
                    driverData.updatedOn = datenow;
                    db.collection('driver').update({ _id: new ObjectId(id) }, { $set: driverData }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });
                        }
                    });
                }
            });
        },
        inactiveAccount: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;

            db.collection('companyUsers').findOne({ _id: new ObjectId(id) }, { password: 0 }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "No user found with the provided Id"
                    });
                } else {
                    db.collection('companyUsers').update({ _id: new ObjectId(id) }, { $set: { "status": "inactive" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });
                        }
                    });
                }
            });



        },
        deleteDriver: function(req, res) {
            req.checkParams('id', 'id must exist').notEmpty();
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('driver').findOne({ _id: new ObjectId(id) }, function(err, result) {
                console.log(result);
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                }
                db.collection('driver').remove({ _id: new ObjectId(id) }, function(err, result) {
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        return res.send({
                            err: false,
                            result: result
                        });
                    }
                });
            });
        },
        getAllDriver: function(req, res) {
            db.collection('driver').find({}).toArray(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "Can not get driver list"
                    });
                } else {
                    res.send({
                        err: false,
                        result: {
                            driver: result
                        }
                    });
                }
            });
        },
        uploadImage: function(req, res) {


            upload(req, res, function(err) {
                if (err) {
                    return res.end("Error uploading file.");
                }
                //res.end("File is uploaded");
                else {
                    res.send({
                        err: false,
                        result: img
                    });
                }





            });

        }
    };
};