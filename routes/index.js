var express = require("express");
var app = express();
module.exports = function(db, app) {

    var adminauthentication = require('../helper/token_auth.js')(db, 'admin');

    //  Driver routes

    var driver = require('../controller/driver.js')(db);

    app.post('/driver', driver.addDriver); //Add user
    app.get('/getDriver/:id', adminauthentication.ensureAuthentication, driver.getDriver); //get user
    app.post('/driver/forgotpassword/sms', driver.forgotpasswordSMS); //forgot password
    app.post('/driver/Changepassword', driver.ChangePassword); //change password by sms
    app.put('/updateDriver/:id', adminauthentication.ensureAuthentication, driver.updateDriver); //update driver
    //    app.delete('/deleteDriver/:id', adminauthentication.ensureAuthentication, driver.deleteDriver);

    app.get('/Driver', driver.getAllDriver); // select all driver
    app.post('/driver/driverlogin', driver.driverLogin); //driver login
    app.post('/driver/driverlogout/:id', driver.driverLogout); //Driver Logout
    app.post('/driver/uploadimage', driver.uploadImage) // uploading image




    // Agent routes

    var agent = require('../controller/agent.js')(db);

    app.post('/agent', agent.addAgent); // add Agent
    app.get('/agent/getAgent/:id', adminauthentication.ensureAuthentication, agent.getAgent); // Get Agent
    app.post('/agent/forgotpassword/sms', agent.forgotpasswordSMS); // forgotpassword through SMS
    app.post('/agent/Changepassword', agent.ChangePassword); // change password through otp
    // app.delete('/deleteAgent/:id', adminauthentication.ensureAuthentication, agent.deleteAgent);
    app.put('/agent/updateagent/:id', adminauthentication.ensureAuthentication, agent.updateAgent); //update driver
    app.post('/agent/agentlogin', agent.agentLogin); //agent login
    app.post('/agent/agentlogout/:id', agent.agentLogout); //agent Logout
    app.delete('/agent/deletedriver/:id', agent.deleteDriver); // Delete driver by Agent
    app.post('/agent/driverActivate/:id', agent.driverActivate); // activate drivers


    //admin routes
    var admin = require('../controller/admin.js')(db);

    app.post('/admin', admin.addAdmin); // add admin
    app.get('/getadmin/:id', adminauthentication.ensureAuthentication, admin.getAdmin); // get admin 
    app.post('/admin/activate/:id', admin.adminActivate); // admin activate 
    app.post('/admin/login', admin.adminLogin); //login admin
    app.post('/admin/logout/:id', admin.adminlogout); // Logout admin
    app.post('/admin/forgotpassword/email', admin.forgotpassword); //forgot password by email get otp
    app.post('/admin/forgotpassword/sms', admin.forgotpasswordSMS); //forgot password by sms get otp
    app.post('/admin/Changepassword', admin.ChangePassword); //change password
    app.put('/admin/updateAdmin/:id', adminauthentication.ensureAuthentication, admin.updateAdmin); //Update Admin 
    app.post('/admin/agentactivate/:id', admin.agentActivate); // Agent activate by admin
    app.delete('/admin/deleteagent/:id', admin.deleteAgent); // Agent Delete by Admin
};