var express = require("express");
var app = express();
require("./config/express.js")(app);
var cors = require('cors');
const port = process.env.PORT || '3000';
app.set('port', port);
app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

require("./config/mongo.js")(function(err, db) {
    if (err) throw err;
    require("./routes/index.js")(db, app);

    app.listen(port);
    console.log("Application listening on port ", port);
});

module.exports = app;