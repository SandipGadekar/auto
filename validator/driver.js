module.exports = {
    "driver.firstName": { isString: true, notEmpty: false },
    "driver.lastName": { isString: true, notEmpty: false },
    "driver.age": { isString: true, notEmpty: false },
    "driver.gender": { isString: true, notEmpty: false },
    "driver.mobile": { isString: true, notEmpty: false },
    "driver.batchno": { isString: true, notEmpty: false },
    "driver.licenceNumber": { isString: true, notEmpty: false },
    "driver.adharNumber": { isString: true, notEmpty: false },
    "driver.licenceImage": { isString: true, notEmpty: false },
    "driver.adharImage": { isString: true, notEmpty: false },
    "driver.driverImage": { isString: true, notEmpty: false },
    "driver.password": { isString: true, notEmpty: false },

};