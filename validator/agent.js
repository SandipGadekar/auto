module.exports = {
    "agent.firstName": { isString: true, notEmpty: false },
    "agent.lastName": { isString: true, notEmpty: false },
    "agent.mobile": { isString: true, notEmpty: false },
    "agent.code": { isString: true, notEmpty: false },
    "agent.password": { isString: true, notEmpty: false },
};